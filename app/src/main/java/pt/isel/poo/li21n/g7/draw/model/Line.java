package pt.isel.poo.li21n.g7.draw.model;

import java.io.PrintWriter;
import java.util.Scanner;

public class Line extends Figure {
    public static final char LETTER = 'L';
    private int x, y;

    public Line(int x, int y){
        this.x = x;
        this.y = y;
    }

    public Line(){}

    @Override
    protected char getLetter(){
        return LETTER;
    }

    @Override
    public void load(Scanner in) {
        super.load(in);
    }

    @Override
    public void save(PrintWriter out) {
        super.save(out);
    }

    public Point getEnd(){
        return null;
    }
    @Override
    public void setEnd(int x, int y) {
        super.setEnd(x, y);
    }

}
