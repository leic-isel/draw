package pt.isel.poo.li21n.g7.draw.model;

import java.io.PrintWriter;
import java.util.Scanner;

public class Point {
    private int x, y;

    Point(int x, int y){
        this.x = x;
        this.y = y;
    }

    Point(){}

    void set(int x, int y){

    }

    public int getX(){ return x; }
    public int getY(){ return y; }

    public void save(PrintWriter out){

    }

    public void load(Scanner in){

    }
}
