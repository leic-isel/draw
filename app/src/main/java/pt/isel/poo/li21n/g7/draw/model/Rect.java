package pt.isel.poo.li21n.g7.draw.model;

public class Rect extends Line {
    public static final char LETTER = 'R';
    private int x, y;

    public Rect(int x, int y){
        this.x = x;
        this.y = y;
    }

    public Rect(){}

    @Override
    protected char getLetter(){
        return LETTER;
    }

}
