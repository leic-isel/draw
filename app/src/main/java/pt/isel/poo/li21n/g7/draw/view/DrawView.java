package pt.isel.poo.li21n.g7.draw.view;

import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.View;

import pt.isel.poo.li21n.g7.draw.DrawController;

public class DrawView extends View {

    public DrawView(DrawController ctrl) {
        super(ctrl);
    }

    public void reloadModel(DrawController model){

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }
}
