package pt.isel.poo.li21n.g7.draw.model;

import java.io.PrintWriter;
import java.util.Scanner;

public class Circle extends Figure {
    public static final char LETTER = 'C';
    private int radius;
    private int x, y;

    public Circle(int x, int y){
        this.x = x;
        this.y = y;
    }

    public Circle(){}

    @Override
    protected char getLetter(){
        return LETTER;
    }

    @Override
    public void load(Scanner in) {
        super.load(in);
    }

    @Override
    public void save(PrintWriter out) {
        super.save(out);
    }

    @Override
    public void setEnd(int x, int y) {
        super.setEnd(x, y);
    }

    public int getRadius() {
        return radius;
    }
}
