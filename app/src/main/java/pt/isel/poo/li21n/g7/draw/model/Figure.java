package pt.isel.poo.li21n.g7.draw.model;

import java.io.PrintWriter;
import java.util.Scanner;

public abstract class Figure {
    private int x, y;

    protected Figure(int x, int y){
        this.x = x;
        this.y = y;
    }

    protected Figure(){ }

    public void save (PrintWriter out){

    }

    public void load (Scanner in){

    }

    protected abstract char getLetter();

    public static Figure newInstance(char letter){
        return null;
    };

    public Point getStart(){
        return null;
    }

    public void setEnd(int x, int y){

    }
}
