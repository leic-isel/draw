package pt.isel.poo.li21n.g7.draw.view;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import pt.isel.poo.li21n.g7.draw.model.Figure;

public abstract class FigureView {

    private Figure f;

    //TODO
    Paint paint;
    {
        paint.setColor(Color.BLACK);
        // size
    }

    FigureView(Figure f){
        this.f = f;
    }

    abstract void draw(Canvas c);

    static FigureView newInstance(Figure f) {
        return null;
    }


}
