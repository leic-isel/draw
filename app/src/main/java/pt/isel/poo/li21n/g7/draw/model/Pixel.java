package pt.isel.poo.li21n.g7.draw.model;

public class Pixel extends Figure {
    public static final char LETTER = 'P';
    private int x, y;

    public Pixel(int x, int y){
        this.x = x;
        this.y = y;
    }

    public Pixel(){}

    @Override
    protected char getLetter(){
        return LETTER;
    }

    @Override
    public void setEnd(int x, int y) {
        super.setEnd(x, y);
    }

}
