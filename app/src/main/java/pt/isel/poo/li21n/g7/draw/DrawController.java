package pt.isel.poo.li21n.g7.draw;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;

import pt.isel.poo.li21n.g7.draw.model.Figure;
import pt.isel.poo.li21n.g7.draw.view.DrawView;

public class DrawController extends Activity {

    private String FILE;

    private Button save, load, reset;
    private DrawView view;
    private RadioButton line, rect, pixel, circle;
    @Override
    protected void onCreate(Bundle State) {
        super.onCreate(State);

        save = new Button(this);
        save.setText("Save");
        load = new Button(this);
        load.setText("Load");
        reset = new Button(this);
        reset.setText("Reset");
        line = new RadioButton(this);
        line.setText("Line");
        rect = new RadioButton(this);
        rect.setText("Rect");
        pixel = new RadioButton(this);
        pixel.setText("Pixel");
        circle = new RadioButton(this);
        circle.setText("Circle");



        LinearLayout main = new LinearLayout(this);
        main.setOrientation(LinearLayout.VERTICAL);
        LinearLayout buttons = new LinearLayout(this);
        buttons.setOrientation(LinearLayout.HORIZONTAL);
        RadioGroup radioButtonsLayout = new RadioGroup(this);
        radioButtonsLayout.setOrientation(LinearLayout.HORIZONTAL);
        view = new DrawView(this);
        view.setBackgroundColor(Color.GRAY);

        buttons.addView(save);
        buttons.addView(load);
        buttons.addView(reset);
        radioButtonsLayout.addView(line);
        radioButtonsLayout.addView(rect);
        radioButtonsLayout.addView(pixel);
        radioButtonsLayout.addView(circle);
        main.addView(buttons);
        main.addView(radioButtonsLayout);
        main.addView(view);

        setContentView(main);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSave();
            }
        });

        load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLoad();
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onReset();
            }
        });

    }

    //TODO
    private void onReset() {
    }

    //TODO
    private void onLoad() {

    }

    //TODO
    private void onSave() {

    }

    public Figure createSelectedFigure(int x, int y){
        return null;
    }

    //TODO

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    //TODO
    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }
}
